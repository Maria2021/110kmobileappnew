package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    String titles[],descriptions[];
    int images[] = {R.drawable.pizza,R.drawable.user,R.drawable.pizza,
            R.drawable.pizza,R.drawable.user,R.drawable.pizza,R.drawable.pizza,
            R.drawable.user,R.drawable.pizza,R.drawable.user};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        recyclerView = findViewById(R.id.pizzalist);
        titles = getResources().getStringArray(R.array.titlepizza);
        descriptions = getResources().getStringArray(R.array.descriptionpizza);
        MyRecyclerViewAdapter recyclerViewAdapter = new MyRecyclerViewAdapter(titles,descriptions,images,this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    public void profileClick(View view) {
        Intent intent = new Intent(MenuActivity.this,ProfileActivity.class);
        startActivity(intent);

    }
}